// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <iostream>

int main()
{
  std::cout << "sizeof(bool):\t" << sizeof(bool) << '\n';
  std::cout << "sizeof(char):\t" << sizeof(char) << '\n';
  std::cout << "sizeof(short):\t" << sizeof(short) << '\n';
  std::cout << "sizeof(int):\t" << sizeof(int) << '\n';
  std::cout << "sizeof(long):\t" << sizeof(long) << '\n';
  std::cout << "sizeof(long long):\t" << sizeof(long long) << '\n';

  std::cout << "sizeof(float):\t" << sizeof(float) << '\n';
  std::cout << "sizeof(double):\t" << sizeof(double) << '\n';
  std::cout << "sizeof(long double):\t" << sizeof(long double) << '\n';

  std::cout << "sizeof(void*):\t" << sizeof(void*) << '\n';

  std::cout << std::endl; // force fflush
}
