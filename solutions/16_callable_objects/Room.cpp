// -----------------------------------------------------------------------------
// Room.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Room.h"
#include "Lamp.h"
#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>

class check_on {
public:
  bool operator()(const Home::Switchable* switchable)
  {
    return switchable->is_on();
  }
};

namespace Home
{
  Room::Room(std::string_view str) : devices{}, name{ str } {}

  bool Room::add(Switchable& switchable)
  {
    devices.push_back(&switchable);
    return true;
  }

  void Room::all_on()
  {
    for (auto device : devices) {
      device->on();
    }
  }

  void Room::all_off()
  {
    for (auto device : devices) {
      device->off();
    }
  }

  void Room::status()
  {
    if (!name.empty()) { std::cout << "In the " << name << " "; }

    auto on =
      std::count_if(std::begin(devices),
                    std::end(devices),
                    [](auto switchable) { return switchable->is_on(); });

    std::cout << "there are " << on << " devices on and "
              << devices.size() - static_cast<std::size_t>(on) << " devices off\n";
  }

  void Room::set_name(std::string_view str)
  {
    name = str;
  }

} // namespace Home
