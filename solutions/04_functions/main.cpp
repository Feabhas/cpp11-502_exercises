// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <cassert>
#include <cstdint> // C99
#include <iostream>
#include <string> // std::to_string

enum class House : std::uint8_t { INVALID, A, B, C, D, E, F, G, H };

const char* HouseName = "-ABCDEFGH"; // classic C technique

using Unit = std::uint8_t; // C++11
constexpr Unit Unit_min{ 1 };
constexpr Unit Unit_max{ 16 };

struct Device
{
  House house;
  Unit  unit;
};

struct Lamp
{
  Device device;
  bool   state;
};

// Output a Lamp's data:
void print_lamp(const Lamp& lamp);

void Lamp_on(Lamp& lamp)
{
  lamp.state = true;
  print_lamp(lamp);
  std::cout << " is on" << std::endl;
}

void Lamp_off(Lamp& lamp)
{
  lamp.state = false;
  print_lamp(lamp);
  std::cout << " is off" << std::endl;
}

Lamp make_lamp(House house, Unit unit)
{
  assert(unit >= Unit_min && unit <= Unit_max);
  return Lamp{ { house, unit }, false };
}

int main()
{
  Lamp desk_lamp{ { House::A, 1 }, false };
  auto lamp = make_lamp(House::A, 2);

  Lamp_on(desk_lamp);
  Lamp_on(lamp);

  Lamp_off(lamp);
  Lamp_off(desk_lamp);
}

// Output a Lamp's data:
//
void print_lamp(const Lamp& lamp)
{
  std::cout << "Lamp(" << HouseName[static_cast<int>(lamp.device.house)]
            << std::to_string(lamp.device.unit) << ")";
}
