// -----------------------------------------------------------------------------
// Module.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Module.h"
#include <array>
#include <iostream>

static const char* HouseName = "-ABCDEFGH"; // classic C technique

// -----------------------------------------------------------
// Member function definitions
//
namespace Home
{
  void Module::switch_on()
  {
    state = true;
    print();
    std::cout << " is on" << std::endl;
  }

  void Module::switch_off()
  {
    state = false;
    print();
    std::cout << " is off" << std::endl;
  }

  bool Module::is_on() const
  {
    return state;
  }

  void Module::status() const
  {
    print();
    std::cout << " is " << (state ? "on" : "off") << std::endl;
  }

  void Module::set_id(const Device& dev)
  {
    device = dev;
  }

  Device Module::id() const
  {
    return device;
  }

  void Module::print() const
  {
    std::cout << HouseName[static_cast<int>(device.house)]
              << std::to_string(device.unit);
  }

} // namespace Home
