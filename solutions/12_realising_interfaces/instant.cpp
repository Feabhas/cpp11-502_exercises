// -----------------------------------------------------------------------------
// Instant.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Instant.h"
#include <iomanip>
#include <iostream>

namespace Timing
{
  Instant::Instant(Hour hr, Minute min) :
    hour{ static_cast<Hour>(hr % 24) }, minute{ static_cast<Minute>(min % 60) }
  {
  }

  bool Instant::is_equal(const Instant& rhs) const
  {
    return ((hour == rhs.hour) && (minute == rhs.minute));
  }

  void Instant::print() const
  {
    std::cout << "\nTime: " << std::setw(2) << std::setfill('0')
              << static_cast<int>(hour) << ':' << std::setw(2)
              << std::setfill('0') << static_cast<int>(minute) << '\n';
  }
} // namespace Timing