// -----------------------------------------------------------------------------
// Lamp.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <cassert>
#include <iostream>
#include <string>

static const char* HouseName = "-ABCDEFGH"; // classic C technique

// -----------------------------------------------------------
// Member function definitions
//
namespace Home
{
  Lamp::Lamp()
  {
    std::cout << "Lamp::Lamp()\n";
  }

  Lamp::Lamp(House house, Unit unit, bool state) :
    device{ house, unit }, state{ state }
  {
    std::cout << "Lamp::Lamp(" << HouseName[static_cast<int>(device.house)]
              << std::to_string(unit) << ")\n";
  }

  Lamp::~Lamp()
  {
    std::cout << "Lamp::~Lamp(" << HouseName[static_cast<int>(device.house)]
              << std::to_string(device.unit) << ")\n";
    off();
  }

  //--------------------------------------

  void Lamp::on()
  {
    state = true;
    print_lamp();
    std::cout << " is on" << std::endl;
  }

  void Lamp::off()
  {
    state = false;
    print_lamp();
    std::cout << " is off" << std::endl;
  }

  bool Lamp::is_on()
  {
    return state;
  }

  void Lamp::status()
  {
    print_lamp();
    std::cout << " is " << (state ? "on" : "off") << std::endl;
  }

  void Lamp::set_id(const Device& dev)
  {
    device = dev;
  }

  Device Lamp::id()
  {
    return device;
  }

  void Lamp::print_lamp()
  {
    std::cout << "Lamp(" << HouseName[static_cast<int>(device.house)]
              << std::to_string(device.unit) << ")";
  }

  Lamp make_lamp(House house, Unit unit)
  {
    assert(unit >= Unit_min && unit <= Unit_max);
    return Lamp{ house, unit, false };
  }

} // namespace Home
