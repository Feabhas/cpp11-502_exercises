// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include <array>
#include <iostream>

using Home::House;
using Home::Lamp;

constexpr unsigned size{ 8 };

void     all_lamps_on(std::array<Lamp, size>& lamps);
void     all_lamps_off(std::array<Lamp, size>& lamps);
unsigned count_lamps_on(std::array<Lamp, size>& lamps);

int main()
{
  std::array<Lamp, size> lamps{
    Lamp{ { House::A, 1 }, false }, Lamp{ { House::A, 2 }, true },
    Lamp{ { House::A, 3 }, false }, Lamp{ { House::B, 1 }, true },
    Lamp{ { House::C, 1 }, false },
  };

  std::cout << "Start: " << count_lamps_on(lamps) << " lamps on" << std::endl;
  all_lamps_on(lamps);
  std::cout << "All on: " << count_lamps_on(lamps) << " lamps on" << std::endl;
  all_lamps_off(lamps);
  std::cout << "All off: " << count_lamps_on(lamps) << " lamps on" << std::endl;
}

//-------------------------------------------------

void all_lamps_on(std::array<Lamp, size>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.house != House::INVALID) { Lamp_on(lamp); }
  }
}

void all_lamps_off(std::array<Lamp, size>& lamps)
{
  for (auto& lamp : lamps) {
    if (lamp.device.house != House::INVALID) { Lamp_off(lamp); }
  }
}

unsigned count_lamps_on(std::array<Lamp, size>& lamps)
{
  unsigned count{};
  for (auto& lamp : lamps) {
    if (lamp.device.house != House::INVALID && lamp.state) { ++count; }
  }
  return count;
}
