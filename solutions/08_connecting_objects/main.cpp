// -----------------------------------------------------------------------------
// main.cpp
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Lamp.h"
#include "Room.h"
#include <cstdint>
#include <iostream>

int main()
{
  auto desk_lamp     = Home::make_lamp(Home::House::A, 2);
  auto standard_lamp = Home::make_lamp(Home::House::A, 3);
  auto bedside_lamp  = Home::make_lamp(Home::House::B, 1);

  Home::Room lounge{ "lounge" };
  lounge.status();
  lounge.add(desk_lamp);
  lounge.add(standard_lamp);

  lounge.all_on();
  lounge.status();

  lounge.all_off();
  lounge.status();

#if 0
	Home::Room bedroom { "bedroom" };
    bedroom.status();
	bedroom.add(bedside_lamp);
	
	bedroom.all_on();
    bedroom.status();
	bedroom.all_off();
    bedroom.status();
#endif
}
