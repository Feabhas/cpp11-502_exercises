// -----------------------------------------------------------------------------
// Module.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef MODULE_H_
#define MODULE_H_

#include "Device.h"

namespace Home
{
  class Module {
  public:
    Module() = default;
    Module(House house, Unit unit) : device{ house, unit } {}
    virtual ~Module() = default;

    void   set_id(const Device& device);
    Device id() const;

    virtual void on()  = 0;
    virtual void off() = 0;
    bool         is_on() const;

    void status() const;

  protected:
    void switch_on();
    void switch_off();

  private:
    void print() const;

    Device device{};
    bool   state{};
  };

} // namespace Home

#endif // MODULE_H_
